<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $table = 'master_barang';

    public function scopeFilter($query, array $filters)
    {
        $query->when($filters['nama_barang'] ?? false, function($query, $nama_barang) {
            $query->where('nama_barang', 'like', '%' . $nama_barang . '%');
        });

        $query->when($filters['harga_satuan'] ?? false, function($query, $harga_satuan, $operator) {
            $query->where('harga_satuan', $operator, $harga_satuan);
        });
    }
}
