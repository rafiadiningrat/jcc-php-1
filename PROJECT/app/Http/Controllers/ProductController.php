<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::query()
            ->when(request()->has('nama_barang') ?? false, function($query) {
                $query->where('nama_barang', 'like', '%' . request('nama_barang') . '%');
            })
            ->when(request()->has('harga_satuan') AND ! empty(request('harga_satuan')) ?? false, function($query) {
                $query->where('harga_satuan', request('operator'), request('harga_satuan'));
            })
            ->latest()
            ->get();

        return view('product.index', [
            'products' => $products,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'nama_barang' => 'required|string|min:2',
            'harga_satuan' => 'required|integer',
        ]);

        Product::create([
            'nama_barang' => $validated['nama_barang'],
            'harga_satuan' => $validated['harga_satuan'],
        ]);

        flash('Data Produk berhasil ditambah!');

        return redirect()->route('product.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        return view('product.edit', [
            'product' => $product
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $validated = $request->validate([
            'nama_barang' => 'required|string|min:2',
            'harga_satuan' => 'required|integer',
        ]);

        $product->update([
            'nama_barang' => $validated['nama_barang'],
            'harga_satuan' => $validated['harga_satuan'],
        ]);

        flash('Data Produk berhasil disimpan!');

        return redirect()->route('product.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();

        flash('Produk berhasil dihapus!');

        return back();
    }
}
