<?php

namespace Database\Seeders;

use App\Models\Product;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::factory()->create([
            'name' => 'Administrator',
            'username' => 'administrator',
            'email' => 'admin@example.com',
            'password' => bcrypt('password'),
            'role' => 'admin',
        ]);

        $kasir = User::factory()->create([
            'name' => 'Kasir',
            'username' => 'kasir1',
            'email' => 'kasir@example.com',
            'password' => bcrypt('password'),
        ]);

        Product::insert([
            [
                'id' => 1,
                'nama_barang' => 'Sabun batang',
                'harga_satuan' => 3000,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'id' => 2,
                'nama_barang' => 'Mi Instan',
                'harga_satuan' => 2000,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'id' => 3,
                'nama_barang' => 'Pensil',
                'harga_satuan' => 1000,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'id' => 4,
                'nama_barang' => 'Kopi sachet',
                'harga_satuan' => 1500,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'id' => 5,
                'nama_barang' => 'Air minum galon',
                'harga_satuan' => 20000,
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ]);
    }
}
