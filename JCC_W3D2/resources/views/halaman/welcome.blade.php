<!DOCTYPE html>
<html lang="en">
@extends('layout.master')
@section('judul')
    Halaman Selamat Datang
@endsection
@section('content')
    <h2>Selamat Datang {{$namaDepan}} {{$namaBelakang}}</h2><br><br>
    <h3>Terima kasih telah bergabung di SanberBook. Social Media kita bersama!</h3>
@endsection