<!DOCTYPE html>
<html lang="en">
@extends('layout.master')
@section('judul')
    Halaman Registrasi
@endsection
@section('content')
    <h1>Buat Account Baru</h1>
    <h3>Sign Up Form</h3>
    <form action="/kirim" method="post">
        @csrf
        <label >First Name:</label> <br>
        <input type="text" name="namaDepan" required> <br> <br>
        <label >Last Name:</label> <br>
        <input type="text" name="namaBelakang" required> <br> <br>
        <label for="">Gender</label><br><br>
        <input type="radio" name="gender" required value="1">Male <br>
        <input type="radio" name="gender" required value="2">Female <br><br>
        <label>Nationality</label>
        <select name="nationality" required><br><br>
            <option value="algeria">Indonesia</option>
            <option value="indonesia">Malaysia</option>
            <option value="zimbabwe">Thailand</option>
        </select> <br><br>
        <label>Language Spoken</label><br><br>
        <input type="checkbox" name="bahasa" >Bahasa Indonesia <br>
        <input type="checkbox" name="bahasa" >English <br>
        <input type="checkbox" name="bahasa" >Other <br><br>
        <label >Bio</label><br><br>
        <textarea name="message" cols="30" rows="10" required></textarea>
        <br><br>
        <input type="submit" value="Sign Up">

    </form>
@endsection
</html>